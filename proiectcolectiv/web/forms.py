from django import forms
from .models import Profile, Job, Category, Location, Review


class EditProfileForm(forms.Form):
    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user')
        profile = Profile.objects.get(user=user)

        super(EditProfileForm, self).__init__(*args, **kwargs)

        self.fields['first_name'] = forms.CharField(required=True,
                                                    widget=forms.TextInput(
                                                        attrs={'class': 'input inputfield',
                                                               'placeholder': user.first_name}),
                                                    max_length=100,
                                                    initial=user.first_name,
                                                    )

        self.fields['last_name'] = forms.CharField(required=True,
                                                   widget=forms.TextInput(
                                                       attrs={'class': 'input inputfield',
                                                              'placeholder': user.last_name}),
                                                   max_length=100,
                                                   initial=user.last_name,
                                                   )

        self.fields['email'] = forms.CharField(required=True,
                                               widget=forms.TextInput(
                                                   attrs={'class': 'input inputfield',
                                                          'placeholder': user.email}),
                                               max_length=100,
                                               initial=user.email,
                                               )

        self.fields['old_password'] = forms.CharField(required=True,
                                                      widget=forms.PasswordInput(
                                                          attrs={'class': 'input inputfield',
                                                                 'placeholder': '*****'}),
                                                      )

        self.fields['new_password'] = forms.CharField(required=False,
                                                      widget=forms.PasswordInput(
                                                          attrs={'class': 'input inputfield',
                                                                 'placeholder': '*****', }),
                                                      )

        self.fields['new_password_confirm'] = forms.CharField(required=False,
                                                              widget=forms.PasswordInput(
                                                                  attrs={'class': 'input inputfield',
                                                                         'placeholder': '*****', }),
                                                              )


class EditInterestForm(forms.Form):
    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user')
        profile = Profile.objects.get(user=user)

        self.CHOICES = [(0, '---')]
        for category in Category.objects.all():
            self.CHOICES.append((category.id, category))

        self.LOCATIONS = [(0, '---')]
        for location in Location.objects.all():
            self.LOCATIONS.append((location.id, location))

        super(EditInterestForm, self).__init__(*args, **kwargs)
        self.fields['interests'] = forms.MultipleChoiceField(required=True,
                                                             widget=forms.Select(
                                                                 attrs={'class': 'inputfield selectfield',
                                                                        }, ),
                                                             choices=self.CHOICES)

        self.fields['location'] = forms.MultipleChoiceField(required=True,
                                                            widget=forms.Select(
                                                                attrs={'class': 'inputfield selectfield',
                                                                       }, ),
                                                            initial=profile.location.id,
                                                            choices=self.LOCATIONS)

    category = forms.MultipleChoiceField(required=True, widget=forms.Select(), choices=[])


class AddJobForm(forms.Form):
    def __init__(self, *args, **kwargs):
        self.CAT_CHOICES = []
        for category in Category.objects.all():
            self.CAT_CHOICES.append((category.id, category))

        self.LOC_CHOICES = []
        for location in Location.objects.all():
            self.LOC_CHOICES.append((location.id, location))

        super(AddJobForm, self).__init__(*args, **kwargs)
        self.fields['title'] = forms.CharField(required=True,
                                               widget=forms.TextInput(
                                                   attrs={'class': 'input inputfield',
                                                        'placeholder': 'Title',
                                                   }),
                                               max_length=100
                                               )

        self.fields['description'] = forms.CharField(required=True,
                                                     widget=forms.Textarea(
                                                         attrs={'class': 'textarea inputfield',
                                                                'placeholder': 'Description',
                                                                }),
                                                     max_length=500
                                                     )

        self.fields['category'] = forms.MultipleChoiceField(required=True,
                                                            widget=forms.Select(

                                                                attrs={'class': 'inputfield selectfield',
                                                                }),
                                                            label="Category",
                                                            choices=self.CAT_CHOICES)

        self.fields['start_date'] = forms.DateField(required=True,

                                                    widget=forms.DateInput(
                                                        attrs={'class': 'input inputfield datepicker',
                                                             'placeholder': 'dd/mm/yyyy',
                                                        }),
                                                    )

        self.fields['end_date'] = forms.DateField(required=True,

                                                  widget=forms.DateInput(
                                                      attrs={'class': 'input inputfield datepicker',
                                                             'placeholder': 'dd/mm/yyyy',
                                                    }),
                                                  )

        self.fields['number_of_hours'] = forms.CharField(required=True,
                                                         widget=forms.NumberInput(
                                                             attrs={'class': 'input inputfield',
                                                                'Placeholder': 'Hours per'}),
                                                         )

        self.fields['per_time'] = forms.MultipleChoiceField(required=True,
                                                            widget=forms.Select(
                                                                attrs={
                                                                    'class': 'inputfield selectfield',
                                                                },
                                                            ),
                                                            choices=Job.PER_TIME_CHOICES
                                                            )

        self.fields['cost_of_services'] = forms.CharField(required=False,
                                                          widget=forms.NumberInput(
                                                              attrs={'class': 'input inputfield',
                                                                    'placeholder': 'Cost per'
                                                              }
                                                          ),
                                                          )

        self.fields['per_cost'] = forms.MultipleChoiceField(required=True,
                                                            widget=forms.Select(
                                                                attrs={'class': 'inputfield selectfield',
                                                                },
                                                            ),
                                                            choices=Job.PER_COST_CHOICES
                                                            )

        self.fields['location'] = forms.MultipleChoiceField(required=True,
                                                            widget=forms.Select(
                                                                attrs={'class': 'inputfield selectfield',
                                                                },
                                                            ),
                                                            choices=self.LOC_CHOICES)

        self.fields['requirements'] = forms.CharField(widget=forms.Textarea(
                                                          attrs={'class': 'textarea inputfield',
                                                          }),
                                                      max_length=500
                                                      )

    category = forms.MultipleChoiceField(required=True, widget=forms.Select(), choices=[])
    location = forms.MultipleChoiceField(required=True, widget=forms.Select(), choices=[])
    per_time = forms.MultipleChoiceField(required=True, widget=forms.Select(), choices=[])
    per_cost = forms.MultipleChoiceField(required=True, widget=forms.Select(), choices=[])


class EditJobForm(forms.Form):
    def __init__(self, *args, **kwargs):
        self.CAT_CHOICES = []
        for category in Category.objects.all():
            self.CAT_CHOICES.append((category.id, category))

        self.LOC_CHOICES = []
        for location in Location.objects.all():
            self.LOC_CHOICES.append((location.id, location))

        super(EditJobForm, self).__init__(*args, **kwargs)
        self.fields['edit_title'] = forms.CharField(required=True,
                                                    widget=forms.TextInput(
                                                    attrs={'class': 'input inputfield',
                                                    }),
                                                    max_length=100,
                                                    disabled=True,
                                               )

        self.fields['edit_description'] = forms.CharField(required=True,
                                                        widget=forms.Textarea(
                                                         attrs={'class': 'textarea inputfield',
                                                                'placeholder': 'Description'
                                                         }),
                                                        max_length=500,
                                                    )

        self.fields['edit_category'] = forms.MultipleChoiceField(required=True,
                                                            widget=forms.Select(
                                                                attrs={'class': 'inputfield selectfield',
                                                                }),
                                                            disabled=True,
                                                            choices=self.CAT_CHOICES)

        self.fields['edit_start_date'] = forms.DateField(required=True,
                                                        widget=forms.DateInput(
                                                        attrs={'class': 'input inputfield datepicker',
                                                                'placeholder': 'dd/mm/yyyy',
                                                        }),
                                                    )

        self.fields['edit_end_date'] = forms.DateField(required=True,
                                                  widget=forms.DateInput(
                                                      attrs={'class': 'input inputfield datepicker',
                                                             'placeholder': 'dd/mm/yyyy',
                                                        }),
                                                  )

        self.fields['edit_number_of_hours'] = forms.CharField(required=True,
                                                         widget=forms.NumberInput(
                                                             attrs={'class': 'input inputfield',
                                                                }),
                                                         )

        self.fields['edit_per_time'] = forms.MultipleChoiceField(required=True,
                                                            widget=forms.Select(
                                                                attrs={'class': 'inputfield selectfield',
                                                                    }),
                                                            choices=Job.PER_TIME_CHOICES
                                                            )

        self.fields['edit_cost_of_services'] = forms.CharField(required=False,
                                                          widget=forms.NumberInput(
                                                              attrs={'class': 'input inputfield',
                                                                }),
                                                          )

        self.fields['edit_per_cost'] = forms.MultipleChoiceField(required=True,
                                                            widget=forms.Select(
                                                                attrs={'class': 'inputfield selectfield',
                                                                    }),
                                                                choices=Job.PER_COST_CHOICES
                                                            )

        self.fields['edit_location'] = forms.MultipleChoiceField(required=True,
                                                            widget=forms.Select(
                                                                attrs={'class': 'inputfield selectfield',
                                                                    }),
                                                            choices=self.LOC_CHOICES
                                                            )

        self.fields['edit_requirements'] = forms.CharField(required=True,
                                                      widget=forms.Textarea(
                                                          attrs={'class': 'textarea inputfield',
                                                            }),
                                                      )

    edit_category = forms.MultipleChoiceField(required=True, widget=forms.Select(), choices=[])
    edit_location = forms.MultipleChoiceField(required=True, widget=forms.Select(), choices=[])
    edit_per_time = forms.MultipleChoiceField(required=True, widget=forms.Select(), choices=[])
    edit_per_cost = forms.MultipleChoiceField(required=True, widget=forms.Select(), choices=[])

class CategorySearchForm(forms.Form):
    def __init__(self, *args, **kwargs):

        self.CAT_CHOICES = []
        for category in Category.objects.all():
            self.CAT_CHOICES.append((category.id, category))

        super(CategorySearchForm, self).__init__(*args, **kwargs)
        self.fields['filter_category'] = forms.MultipleChoiceField(required=True,
                                               widget=forms.Select(
                                                   attrs={'class': 'inputfield selectfield'}),
                                               label="Category",
                                               choices=[('0', 'All categories')] + self.CAT_CHOICES)

    category = forms.MultipleChoiceField(required=True, widget=forms.Select(), choices=[])

class LocationSearchForm(forms.Form):
    def __init__(self, *args, **kwargs):

        self.CAT_CHOICES = []
        for location in Location.objects.all():
            self.CAT_CHOICES.append((location.id, location))

        super(LocationSearchForm, self).__init__(*args, **kwargs)
        self.fields['filter_location'] = forms.MultipleChoiceField(required=True,
                                               widget=forms.Select(
                                                   attrs={'class': 'inputfield selectfield'},),
                                               label="Location",
                                               choices=[('0', 'All locations')] + self.CAT_CHOICES)

    location = forms.MultipleChoiceField(required=True, widget=forms.Select(), choices=[])

class OrderSearchForm(forms.Form):
    def __init__(self, *args, **kwargs):

        self.CAT_CHOICES = []

        self.CAT_CHOICES.append((1, "Ascending"))
        self.CAT_CHOICES.append((2, "Descending"))

        super(OrderSearchForm, self).__init__(*args, **kwargs)
        self.fields['order'] = forms.MultipleChoiceField(required=True,
                                               widget=forms.Select(
                                                   attrs={'class': 'inputfield selectfield'},),
                                               label="Order",
                                               choices=[('0', '---')] +  self.CAT_CHOICES)

    order = forms.MultipleChoiceField(required=True, widget=forms.Select(), choices=[])

class CriteriaSearchForm(forms.Form):
    def __init__(self, *args, **kwargs):

        self.CAT_CHOICES = []

        self.CAT_CHOICES.append((1, "Name"))
        self.CAT_CHOICES.append((2, "Start time"))
        self.CAT_CHOICES.append((3, "End time"))
        self.CAT_CHOICES.append((4, "Total hours"))
        self.CAT_CHOICES.append((5, "Total currency"))

        super(CriteriaSearchForm, self).__init__(*args, **kwargs)
        self.fields['criteria'] = forms.MultipleChoiceField(required=True,
                                               widget=forms.Select(
                                                   attrs={'class': 'inputfield selectfield'},),
                                               label="Criteria",
                                                            choices=[('0', '---')] + self.CAT_CHOICES)

    criteria = forms.MultipleChoiceField(required=True, widget=forms.Select(), choices=[])

class SearchByHoursForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super(SearchByHoursForm, self).__init__(*args, **kwargs)

        self.fields['filter_min_hours'] = forms.IntegerField(required=False,
                                                      widget=forms.TextInput(
                                                          attrs={'class': 'input',
                                                                 'placeholder': 'hours',
                                                          }),
                                                      label="Minimum nr. hours: ")

        self.fields['filter_max_hours'] = forms.IntegerField(required=False,
                                                      widget=forms.TextInput(
                                                          attrs={'class': 'input',
                                                                 'placeholder': 'hours',
                                                          }),
                                                      label="Maximum nr. hours: ")
        self.fields['filter_per_time'] = forms.MultipleChoiceField(required=False,
                                                            widget=forms.Select(
                                                                attrs={
                                                            }),
                                                            choices=Job.PER_TIME_CHOICES
                                                            )

    per_time = forms.MultipleChoiceField(required=False, widget=forms.Select(), choices=[])


class ReviewForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super(ReviewForm, self).__init__(*args, **kwargs)

        self.fields['stars'] = forms.ChoiceField(required=True,
                                                 widget=forms.Select(
                                                     attrs={'class': 'inputfield selectfield',
                                                        }),
                                                 choices=Review.STAR_CHOICES
                                                 )
        self.fields['comment'] = forms.CharField(required=True,
                                                 widget=forms.Textarea(
                                                     attrs={'class': 'textarea inputfield',
                                                        }),
                                                 )

    stars = forms.ChoiceField(required=True, widget=forms.Select(), choices=[])


class SearchByIntervalForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super(SearchByIntervalForm, self).__init__(*args, **kwargs)

        self.fields['filter_start_date'] = forms.DateField(required=False,
                                                    widget=forms.DateTimeInput(
                                                        attrs={'class': 'input datepicker',
                                                               'placeholder': 'dd/mm/yyyy', }),
                                                    label="Start")

        self.fields['filter_end_date'] = forms.DateField(required=False,
                                                  widget=forms.DateTimeInput(
                                                      attrs={'class': 'input datepicker',
                                                             'placeholder': 'dd/mm/yyyy', }),
                                                  label="End")


class SearchByCostForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super(SearchByCostForm, self).__init__(*args, **kwargs)

        self.fields['filter_min_cost'] = forms.IntegerField(required=False,
                                                     widget=forms.TextInput(
                                                         attrs={'class': 'costField input',
                                                                'placeholder': 'pay'
                                                                }),
                                                     label="Minimum cost: ")

        self.fields['filter_max_cost'] = forms.IntegerField(required=False,
                                                     widget=forms.TextInput(
                                                         attrs={'class': 'costField input',
                                                                'placeholder': 'pay'
                                                                }),
                                                     label="Maximum cost: ")
        self.fields['filter_per_cost'] = forms.MultipleChoiceField(required=False,
                                                            widget=forms.Select(
                                                                attrs={'class': 'costField input',
                                                                       }),
                                                            choices=Job.PER_COST_CHOICES
                                                            )
        self.fields['filter_voluntary'] = forms.BooleanField(required=False,
                                                      widget=forms.CheckboxInput(
                                                          attrs={
                                                              'id': 'voluntaryCB'}
                                                      ))


class UploadProfilePictureForm(forms.Form):
    image = forms.ImageField()

class RegisterForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super(RegisterForm, self).__init__(*args, **kwargs)

        self.LOC_CHOICES = []
        for location in Location.objects.all():
            self.LOC_CHOICES.append((location.id, location))

        self.fields['username'] = forms.CharField(required=True,
                                                widget=forms.TextInput(
                                                        attrs={'class': 'input inputfield',
                                                               'placeholder': 'Username'
                                                            }
                                                    )
                                                )

        self.fields['email'] = forms.CharField(required=True,
                                                widget=forms.TextInput(
                                                        attrs={'class': 'input inputfield',
                                                            'placeholder': 'E-mail'
                                                            }
                                                    )
                                                )

        self.fields['password'] = forms.CharField(required=True,
                                                    widget=forms.PasswordInput(
                                                        attrs={'class': 'input inputfield',
                                                               'placeholder': 'Password'
                                                               }
                                                    )
                                                  )

        self.fields['confirm_password'] = forms.CharField(required=True,
                                                    widget=forms.PasswordInput(
                                                        attrs={'class': 'input inputfield',
                                                               'placeholder': 'Password'
                                                               }
                                                    )
                                                  )

        self.fields['first_name'] = forms.CharField(required=True,
                                                    widget=forms.TextInput(
                                                        attrs={'class': 'input inputfield',
                                                               'placeholder': 'First name'
                                                               }
                                                    ))

        self.fields['last_name'] = forms.CharField(required=True,
                                                    widget=forms.TextInput(
                                                        attrs={'class': 'input inputfield',
                                                               'placeholder': 'Last name'
                                                               }
                                                    ))

        self.fields['location'] = forms.MultipleChoiceField(required=True,
                                                            widget=forms.Select(
                                                                attrs={'class': 'inputfield selectfield',
                                                                },
                                                            ),
                                                            choices=self.LOC_CHOICES)

    location = forms.MultipleChoiceField(required=True, widget=forms.Select(), choices=[])

class LoginForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super(LoginForm, self).__init__(*args, **kwargs)

        self.fields['username'] = forms.CharField(required=True,
                                                  widget=forms.TextInput(
                                                      attrs={'class': 'input inputfield',
                                                             'placeholder': 'Username',
                                                             'id': 'auth-focus'
                                                             }
                                                  )
                                                  )

        self.fields['password'] = forms.CharField(required=True,
                                                  widget=forms.PasswordInput(
                                                      attrs={'class': 'input inputfield',
                                                             'placeholder': 'Password'
                                                             }
                                                  )
                                                  )