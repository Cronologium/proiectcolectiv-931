from django.conf.urls import url, include

from .job import *
from .views import *
from .account import *


urlpatterns = [


    #Account-related
    url(r'^logout$', log_out, name='log out'),
    url(r'^account/login$', auth_ajax, name='authenticate'),
    url(r'^oauth/', include('social_django.urls', namespace='social')),
    url(r'^account/register$', reg_ajax, name='register'),
    url(r'^seekers$', seekers, name='view job seekers'),
    url(r'^accounts/profile/', oauth_login_after),

    #Profile-related
    #url(r'^profile/(?P<username>[^\/]*)$', profile, name='view profile'),
    #url(r'^profile/(?P<username>.*)/reviews$', reviews, name='user reviews'),
    #url(r'^edit$', edit_profile, name='edit profile'),
    #url(r'^edit/addinterest$', edit_interest_profile, name='edit interest profile'),
    #url(r'^notifications$', notifications, name='notifications'),

    #Job-related
    url(r'^jobs$', jobs, name='view jobs'),
    url(r'^job/add$', add_ajax, name='add job ajax'),
    url(r'^job/edit$', edit_job_ajax, name='edit job ajax'),
    url(r'^job/(?P<job_id>[0-9]+)$', job, name='job detail'),
    url(r'^job/(?P<job_id>[0-9]+)/delete$', job_delete, name='job delete'),
    url(r'^job/(?P<job_id>[0-9]+)/apply$', job_apply, name='choose apply'),
    url(r'^job/(?P<job_id>[0-9]+)/choose$', job_choose_applicant, name='choose applicant'),
    url(r'^job/(?P<job_id>[0-9]+)/deselect$', job_deselect_applicant, name='deselect applicant'),
    url(r'^job/(?P<job_id>[0-9]+)/review$', job_review_send_ajax, name='send review'),


    #Profile-related
    url(r'^profile/(?P<username>[^\/]+)$', profile, name='view profile'),
    url(r'^profile/(?P<username>[^\/]+)/jobs$', profile_jobs, name='profile jobs'),
    url(r'^profile/(?P<username>[^\/]+)/dashboard$', profile_dashboard, name='profile interests'),
    url(r'^profile/(?P<username>[^\/]+)/reviews$', profile_reviews, name='profile_reviews'),
    url(r'^profile/(?P<username>[^\/]+)/notifications$', profile_notifications, name='profile notifications'),
    url(r'^profile/(?P<username>[^\/]+)/edit$', profile_edit, name='profile edit'),

    #Links to be replaced
    url(r'^handle_notification/(?P<notification_id>[0-9]+)$', handle_notification, name='handle notification'),

    url(r'^account/edit$', edit_ajax, name='edit profile ajax'),
    url(r'^account/edit/addinterest$', edit_interest_ajax, name='edit interest ajax'),
    url(r'^account/edit/deleteinterest$', delete_interest_profile, name='delete interest profile'),
    url(r'^account/edit/checks$', edit_checks_ajax, name='edit checks ajax'),
    url(r'^check_notifications$', check_for_new_notification, name='check_notifications'),
    url(r'^upload_pic$', upload_pic, name='upload pic'),


    #url(r'^edit$', edit_profile, name='edit profile'),
    #url(r'^edit/addinterest$', edit_interest_profile, name='edit interest profile'),
    #url(r'^seekers$', seekers, name='view job seekers'),
    #url(r'^handle_notification/(?P<notification_id>[0-9]+)$', handle_notification, name='handle notification'),
    #url(r'^notifications$', notifications, name='notifications'),
    #url(r'^job/(?P<job_id>[0-9]+)/review$', job_review, name='job review'),
    #url(r'^job/review/send$', job_review_send_ajax, name='job review send'),

    #Keep this last
    url(r'$', home),
]