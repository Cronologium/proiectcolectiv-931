from django.db.models import Q


def filter_content(array, page, per_page=20):
    max_page = max(1, (len(array) - 1) // per_page + 1)
    if page > max_page:
        return max_page, None, []
    if page < 1:
        return max_page, None, []
    if len(array) > per_page:
        min_job = max(len(array) - per_page * page, 0)
        max_job = min(len(array) - per_page * (page - 1), len(array))
        array = array[len(array) - max_job: len(array) - min_job]

    lowest = max(1, page - 5)
    highest = min(max_page, page + 5)
    page_buttons = [i for i in range(lowest, highest + 1)]
    return max_page, array, page_buttons


def filter_jobs(request, jobs_list):
    return jobs_list.filter(Q(is_available=True) |
                            Q(is_available=False, user__id=request.user.id) |
                            (~Q(user__id=request.user.id)) &
                            Q(is_available=False, applicants__user__id__in=[request.user.id])).distinct()

def filter_category(job_list , categories):
    q_objects = Q()
    for item in categories:
        q_objects.add(Q(category=item), Q.OR)
    return job_list.filter(q_objects)

def filter_location(job_list , locations):
    q_objects = Q()
    for item in locations:
        q_objects.add(Q(location=item), Q.OR)
    return job_list.filter(q_objects)


def filter_jobs_hours(job_list, min_hours, max_hours, per_time):
    if min_hours:
        job_list = job_list.filter(Q(per_time=per_time) & Q(number_of_hours__gte=min_hours))
    if max_hours:
        job_list = job_list.filter(Q(per_time=per_time) & Q(number_of_hours__lte=max_hours))
    return job_list


def filter_jobs_interval(job_list, start, end):
    if start:
        job_list = job_list.filter(Q(start_date__gte=start))
    if end:
        job_list = job_list.filter(Q(end_date__lte=end))
    return job_list


def filter_jobs_cost(job_list, min_cost, max_cost, per_cost, voluntary):
    if voluntary=="on":
        job_list =job_list.filter(Q(cost_of_services=0))
        return job_list
    if min_cost:
        job_list = job_list.filter(Q(per_cost=per_cost) & Q(cost_of_services__gte=min_cost))
    if max_cost:
        job_list = job_list.filter(Q(per_cost=per_cost) & Q(cost_of_services__lte=max_cost))
    return job_list

def filter_user(job_list, user):
    job_list = job_list.filter(Q(user=user))
    return job_list
