
$(document).ready(function () {
    var voluntary=false;

    $('#pay-interest').click(function() {
        voluntary = !voluntary;
        $('#id_cost_of_services').prop('disabled', voluntary);
        $('#id_per_cost').prop('disabled', voluntary);
    });
    $('#add-title-error').text(String.fromCharCode(8192));
    $('#add-description-error').text(String.fromCharCode(8192));
    $('#add-category-error').text(String.fromCharCode(8192));
    $('#add-start-error').text(String.fromCharCode(8192));
    $('#add-end-error').text(String.fromCharCode(8192));
    $('#add-number_of_hours-error').text(String.fromCharCode(8192));
    $('#add-cost_of_services-error').text(String.fromCharCode(8192));
    $('#add-location-error').text(String.fromCharCode(8192));
    $('#add-requirements-error').text(String.fromCharCode(8192));

    $('#add_button').click(function() {
        var data = {};
        var err = false;

        $('#add-title-error').text(String.fromCharCode(8192));
        $('#add-description-error').text(String.fromCharCode(8192));
        $('#add-category-error').text(String.fromCharCode(8192));
        $('#add-start-error').text(String.fromCharCode(8192));
        $('#add-end-error').text(String.fromCharCode(8192));
        $('#add-number_of_hours-error').text(String.fromCharCode(8192));
        $('#add-cost_of_services-error').text(String.fromCharCode(8192));
        $('#add-location-error').text(String.fromCharCode(8192));
        $('#add-requirements-error').text(String.fromCharCode(8192));
        $('#add_form input').each(function() {
            if ($(this).hasClass('inputfield')) {
                data[$(this).attr('name')] = $(this).val();
            }
            else if($(this).attr('name') === 'csrfmiddlewaretoken') {
                data[$(this).attr('name')] = $(this).val();
            }
            else if ($(this).hasClass('checkbox')) {
               data[$(this).attr('name')] = $(this).is(':checked');
            }

        });

        if (voluntary) {
            data['cost_of_services'] = 0;
        }

        data['category'] = $('#id_category').find(':selected').text();
        data['per_time'] = $('#id_per_time').find(':selected').text();
        data['per_cost'] = $('#id_per_cost').find(':selected').text();
        data['location'] = $('#id_location').find(':selected').text();


        $.ajax({

            dataType: 'json',
            type: 'POST',
            url: '/job/add',
            data: data,
            success: function(data) {

                if ('' in data) {
                    window.location.href = '/jobs'
                }
                else {
                    for (key in data)
                    {

                        FlashService.error(key, data[key]);
                    }
                }
            },
            error: function() {
                FlashService.error('add-requirements-error', 'A problem occurred on the server');
            }
        });
    });

    $(".datepicker").datepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: "2017:2030",
        dateFormat: "dd/mm/yy"

    });

});