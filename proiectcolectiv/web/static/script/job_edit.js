
$(document).ready(function () {
    var edit_voluntary=false;

    $('#edit-pay-interest').click(function() {
        edit_voluntary = !edit_voluntary;
        $('#id_edit_cost_of_services').prop('disabled', edit_voluntary);
        $('#id_edit_per_cost').prop('disabled', edit_voluntary);
    });

    $('#edit-title-error').text(String.fromCharCode(8192));
    $('#edit-description-error').text(String.fromCharCode(8192));
    $('#edit-category-error').text(String.fromCharCode(8192));
    $('#edit-start-error').text(String.fromCharCode(8192));
    $('#edit-end-error').text(String.fromCharCode(8192));
    $('#edit-number_of_hours-error').text(String.fromCharCode(8192));
    $('#edit-cost_of_services-error').text(String.fromCharCode(8192));
    $('#edit-location-error').text(String.fromCharCode(8192));
    $('#edit-requirements-error').text(String.fromCharCode(8192));


    function update(id_to_update){
        var data = {};
        console.log(data);
        var err = false;


        $('#edit-title-error').text(String.fromCharCode(8192));
        $('#edit-description-error').text(String.fromCharCode(8192));
        $('#edit-category-error').text(String.fromCharCode(8192));
        $('#edit-start-error').text(String.fromCharCode(8192));
        $('#edit-end-error').text(String.fromCharCode(8192));
        $('#edit-number_of_hours-error').text(String.fromCharCode(8192));
        $('#edit-cost_of_services-error').text(String.fromCharCode(8192));
        $('#edit-location-error').text(String.fromCharCode(8192));
        $('#edit-requirements-error').text(String.fromCharCode(8192));
        $('#job-edit input').each(function() {

            if ($(this).hasClass('inputfield')) {
                data[$(this).attr('name')] = $(this).val();
            }
            else if($(this).attr('name') === 'csrfmiddlewaretoken') {
                data[$(this).attr('name')] = $(this).val();
            }
            else if ($(this).hasClass('checkbox')) {
               data[$(this).attr('name')] = $(this).is(':checked');
            }

        });

        if (voluntary) {
            data['cost_of_services'] = 0;
        }

        data['category'] = $('#id_edit_category').find(':selected').text();
        data['per_time'] = $('#id_edit_per_time').find(':selected').text();
        data['per_cost'] = $('#id_edit_per_cost').find(':selected').text();
        data['location'] = $('#id_edit_location').find(':selected').text();


        $.ajax({

            dataType: 'json',
            type: 'POST',
            url: '/job/edit/' + String(id_to_update),
            data: data,

            success: function(data) {
                console.log(data);
                if ('' in data) {
                    window.location.href = '/jobs'
                }
                else {
                    for (key in data)
                    {
                        console.log(data);
                        FlashService.error(key, data[key]);
                    }
                }
            },
            error: function() {
                FlashService.error('edit-requirements-error', 'A problem occurred on the server');
            }
        });
    }

    $(".datepicker").datepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: "2017:2030",
        dateFormat: "dd/mm/yy"
    });

});