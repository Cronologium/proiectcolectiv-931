var edit_voluntary=false;

function openEditForm(id, title, description, category_id, start_date, end_date, number_of_hours, per_time, cost_of_services, per_cost, location_id, requirements) {
    console.log(id);
    console.log(title);
    console.log(description);
    console.log(category_id);
    console.log(start_date);
    console.log(end_date);
    console.log(number_of_hours);
    console.log(per_time);
    console.log(cost_of_services);
    console.log(per_cost);
    console.log(location_id);
    console.log(requirements);

    $('#job-edit-title').text('Edit "' + title + '"');
    $('#job-edit-id').val(id);
    $('#id_edit_title').val(title);
    $('#id_edit_description').val(description);
    $('#id_edit_start_date').val(start_date);
    $('#id_edit_end_date').val(end_date);
    $('#id_edit_number_of_hours').val(number_of_hours);
    if (parseInt(cost_of_services) === 0) {
        edit_voluntary = !edit_voluntary;
        $('#id_edit_cost_of_services').prop('disabled', edit_voluntary);
        $('#id_edit_per_cost').prop('disabled', edit_voluntary);
        $('#edit-pay-interest').prop('checked', true);
    }
    $('#id_edit_cost_of_services').val(cost_of_services);
    $('#id_edit_requirements').val(requirements);
    $('#id_edit_per_cost option[value=' + per_cost + ']').prop('selected', true);
    $('#id_edit_per_time option[value=' + per_time + ']').prop('selected', true);
    $('#id_edit_category option[value=' + category_id + ']').prop('selected', true);
    $('#id_edit_location option[value=' + location_id + ']').prop('selected', true);

    $('#job-edit').show();
}

$(document).ready(function() {
    $('#job-edit-cancel').click(function(){
        $('#job-edit').hide();
    });
    $('#job-edit-x').click(function(){
        $('#job-edit').hide();
    });

    $('#edit-pay-interest').click(function() {
        edit_voluntary = !edit_voluntary;
        $('#id_edit_cost_of_services').prop('disabled', edit_voluntary);
        $('#id_edit_per_cost').prop('disabled', edit_voluntary);
    });

    $('#edit-title-error').text(String.fromCharCode(8192));
    $('#edit-description-error').text(String.fromCharCode(8192));
    $('#edit-category-error').text(String.fromCharCode(8192));
    $('#edit-start-error').text(String.fromCharCode(8192));
    $('#edit-end-error').text(String.fromCharCode(8192));
    $('#edit-number_of_hours-error').text(String.fromCharCode(8192));
    $('#edit-cost_of_services-error').text(String.fromCharCode(8192));
    $('#edit-location-error').text(String.fromCharCode(8192));
    $('#edit-requirements-error').text(String.fromCharCode(8192));


    $('#job-edit-submit').click(function() {
        var data = {};
        var err = false;

        data['job_id'] = $('#job-edit-id').val();

        $('#edit-title-error').text(String.fromCharCode(8192));
        $('#edit-description-error').text(String.fromCharCode(8192));
        $('#edit-category-error').text(String.fromCharCode(8192));
        $('#edit-start-error').text(String.fromCharCode(8192));
        $('#edit-end-error').text(String.fromCharCode(8192));
        $('#edit-number_of_hours-error').text(String.fromCharCode(8192));
        $('#edit-cost_of_services-error').text(String.fromCharCode(8192));
        $('#edit-location-error').text(String.fromCharCode(8192));
        $('#edit-requirements-error').text(String.fromCharCode(8192));
        $('#job-edit input').each(function() {

            if ($(this).hasClass('inputfield')) {
                data[$(this).attr('name')] = $(this).val();
            }
            else if($(this).attr('name') === 'csrfmiddlewaretoken') {
                data[$(this).attr('name')] = $(this).val();
            }
            else if ($(this).hasClass('checkbox')) {
               data[$(this).attr('name')] = $(this).is(':checked');
            }

        });
        $('#job-edit textarea').each(function() {
           if ($(this).hasClass('inputfield')) {
               data[$(this).attr('name')] = $(this).val();
           }
        });

        if (voluntary) {
            data['cost_of_services'] = 0;
        }

        data['edit_category'] = $('#id_edit_category').find(':selected').text();
        data['edit_per_time'] = $('#id_edit_per_time').find(':selected').text();
        data['edit_per_cost'] = $('#id_edit_per_cost').find(':selected').text();
        data['edit_location'] = $('#id_edit_location').find(':selected').text();

        console.log(data);
        $.ajax({

            dataType: 'json',
            type: 'POST',
            url: '/job/edit',
            data: data,

            success: function(data) {
                console.log(data);
                if ('' in data) {
                    window.location.href = '/jobs'
                }
                else {
                    for (key in data)
                    {
                        FlashService.error(key, data[key]);
                    }
                }
            },
            error: function() {
                FlashService.error('edit-requirements-error', 'A problem occurred on the server');
            }
        });
    });
});