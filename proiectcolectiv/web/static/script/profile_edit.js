$(document).ready(function () {
    $('#edit-firstname-error').text(String.fromCharCode(8192));
    $('#edit-lastname-error').text(String.fromCharCode(8192));
    $('#edit-email-error').text(String.fromCharCode(8192));
    $('#edit-pass-error').text(String.fromCharCode(8192));
    $('#edit-confirm-error').text(String.fromCharCode(8192));
    $('#edit-pass-confirm-error').text(String.fromCharCode(8192));
    $('#edit-interests-error').text(String.fromCharCode(8192));

    if ($('#is_searching').text() === 'True') {
        $('#interested-checkbox').prop("checked", true);
    }
    if ($('#is_offering').text() === 'True') {
        $('#aplicants-checkbox').prop("checked", true);
    }

    var init_text = "No file chosen";
    $('#file-selected').text(init_text);
    var width = (14 / init_text.length) * 30;
    var widthstr = '' + width + '%';
    $('#file-selected').css('margin-left', widthstr);

    $('#image-input').on("change", function() {
        var file = $(this).val().replace(/^.*[\\\/]/, '');
        $('#file-selected').text(file);
        var width = (14 / file.length) * 30;
        var widthstr = '' + width + '%';
        $('#file-selected').css('margin-left', widthstr);
    });

    $('#id_location').val($('#location_hidden').text());

    $('#edit_button').click(function() {
        var data = {};
        var err = false;
        $('#edit-firstname-error').text(String.fromCharCode(8192));
        $('#edit-lastname-error').text(String.fromCharCode(8192));
        $('#edit-email-error').text(String.fromCharCode(8192));
        $('#edit-pass-error').text(String.fromCharCode(8192));
        $('#edit-confirm-error').text(String.fromCharCode(8192));
        $('#edit-pass-confirm-error').text(String.fromCharCode(8192));
        $('#edit-interests-error').text(String.fromCharCode(8192));
        $('#edit_form input').each(function() {
            if ($(this).hasClass('inputfield')) {
                data[$(this).attr('name')] = $(this).val();
            }
            else if($(this).attr('name') === 'csrfmiddlewaretoken') {
                data[$(this).attr('name')] = $(this).val();
            }
            else if ($(this).hasClass('checkbox')) {
                data[$(this).attr('name')] = $(this).is(':checked');
            }
        });

        $.ajax({
            dataType: 'json',
            type: 'POST',
            url: '/account/edit',
            data: data,
            success: function(data) {
                console.log(data);
                if ('' in data) {
                    location.reload();
                }
                else {
                    for (key in data)
                    {
                        console.log(key);
                        FlashService.error(key, data[key]);
                    }
                }
            },
            error: function() {
                FlashService.error('edit-confirm-error', 'A problem occurred on the server');
            }
        });

    });
    $("#aplicants-checkbox").click(function(){
     var data = {};
     var err = false;
     var box = $(this);
     data['is_offering'] = '';
     $('#jobs-general input').each(function() {
            if($(this).attr('name') === 'csrfmiddlewaretoken') {
                data[$(this).attr('name')] = $(this).val();
            }
        });
        $.ajax({
            dataType: 'json',
            type: 'POST',
            url: '/account/edit/checks',
            data: data ,
            success: function() {
                FlashService.success('edit-saved-applicants-error', 'Saved!');
            },
            error: function() {
               //box.val()

            }
        });
    });

$("#interested-checkbox").click(function() {
    var data = {};
    var err = false;
    var box = $(this);
    data['is_searching'] = '';
    $('#jobs-general input').each(function () {
        if ($(this).attr('name') === 'csrfmiddlewaretoken') {
            data[$(this).attr('name')] = $(this).val();
        }
    });
    $.ajax({
        dataType: 'json',
        type: 'POST',
        url: '/account/edit/checks',
        data: data,
        success: function() {
            FlashService.success('edit-saved-interested-error', 'Saved!');
        },
        error: function () {
            //box.val()

        }
    });
});
    $('#edit_interest_button').click(function() {
        var data = {};
        var err = false;
        $('#edit-interests-error').text(String.fromCharCode(8192));
        $('#edit_interest_form input').each(function() {
            if($(this).attr('name') === 'csrfmiddlewaretoken') {
                data[$(this).attr('name')] = $(this).val();
            }
        });
        data['interests'] = $('#id_interests').find(':selected').val();
        data['pay-interest'] = $('#pay-interest').is(':checked');

        $.ajax({
            dataType: 'json',
            type: 'POST',
            url: '/account/edit/addinterest',
            data: data,
            success: function(data) {
                if ('' in data) {
                    location.reload();
                }
                else {
                    for (key in data)
                    {
                        console.log(key);
                        FlashService.error(key, data[key]);
                    }
                }
            },
            error: function() {
                FlashService.error('edit-confirm-error', 'A problem occurred on the server');
            }
        });
    });
 });