import re

import datetime

from django.http import JsonResponse, Http404
from django.shortcuts import redirect

from .models import Interest, Review, Profile
from .models import Job, Category, Location, Notification

class PostMock:
    def __init__(self):
        self.POST = {}

def check_add_fields(request):
    errors = {}
    if 'title' not in request.POST:
        errors['add-title-error'] =  'Please enter title'
    if 'description' not in request.POST:
        errors['add-description-error'] = 'Please enter description'
    if 'category' not in request.POST:
        errors['add-category-error'] = 'Please enter category'
    if 'number_of_hours' not in request.POST:
        errors['add-number_of_hours-error'] = 'Please enter number of hours'
    if 'cost_of_services' not in request.POST:
        errors['add-cost_of_services-error'] = 'Please enter cost of services'
    if 'location' not in request.POST:
        errors['add-location-error'] = 'Please enter location'
    if 'requirements' not in request.POST:
        errors['add-requirements-error'] = 'Please enter requirements'
    if len(errors) == 0:
        return None
    return errors


def validate_title(request):
    if 'title' in request.POST and request.POST['title'] == '':
        return {'add-title-error': 'Please enter title'}
    return None


def validate_description(request):
    if 'description' in request.POST and request.POST['description'] == '':
        return {'add-description-error': 'Please enter description'}
    return None


def validate_category(request):
    if 'category' in request.POST and request.POST['category'] == '':
        return {'add-category-error': 'Please enter category'}
    categories = []
    for cat in Category.objects.all():
        categories.append(cat.name)
    category = request.POST['category']
    if category not in categories:
        return {'add-category-error': 'Please choose a valid category'}
    return None


def validate_date(date):
    y,m,d = date.split('-')
    if int(d) > 31 or int(d) < 1:
        return 'error'
    if int(m) > 12 or int(m) < 1:
        return 'error'
    if int(m) == 2:
        if int(d) > 29:
            return 'error'
    return None


def validate_start_date(request):
    if 'start_date' in request.POST and request.POST['start_date'] == '':
        return {'add-start-error': 'Please enter start date'}
    try:
        start_date = datetime.datetime.strptime(request.POST['start_date'], '%d/%m/%Y')
        #print(start_date)
    except ValueError:
        return {'add-start-error': 'Please enter a valid date'}
    if start_date < datetime.datetime.now():
        return {'add-start-error': 'Start date cannot be in the past'}
    return None


def validate_end_date(request):
    if 'end_date' in request.POST and request.POST['end_date'] == '':
        return {'add-end-error': 'Please enter end date'}
    try:
        end_date = datetime.datetime.strptime(request.POST['end_date'], '%d/%m/%Y')
        print('end',end_date)
    except ValueError:
        return {'add-end-error': 'Please enter a valid date'}
    try:
        start_date = datetime.datetime.strptime(request.POST['start_date'], '%d/%m/%Y')
    except ValueError:
        return {'add-start-error': 'Please enter a valid date'}
    if start_date > end_date:
        return {'add-end-error': 'End date cannot be before start date'}
    return None


def validate_number_of_hours(request):
    hours = request.POST['number_of_hours']
    if 'number_of_hours' in request.POST and request.POST['number_of_hours'] == '':
        return {'add-number_of_hours-error': 'Please enter number of hours'}
    try:
        int(hours)
    except:
        return {'add-number_of_hours-error': 'Number of hours cannot be string'}
    return None


def validate_cost_of_services(request):
    cost = request.POST['cost_of_services']
    if 'cost_of_services' in request.POST and request.POST['cost_of_services'] == '':
        return {'add-cost_of_services-error': 'Please enter cost of services'}

    try:
        float(cost)
    except:
        return {'add-cost_of_services-error': 'Cost cannot be string'}
    return None


def validate_location(request):
    if 'location' in request.POST and request.POST['location'] == '':
        return {'add-location-error': 'Please enter location'}
    locations = []
    for loc in Location.objects.all():
        locations.append(loc.name)
    location = request.POST['location']
    if location not in locations:
        return {'add-location-error': 'Please choose a valid location'}
    return None


def validate_requirements(request):
    return None


def add_ajax(request):
    print (str(request.POST))

    err = False
    errors = {}

    empty_check = check_add_fields(request)
    if empty_check is not None:
        return JsonResponse(empty_check)

    title_check = validate_title(request)
    if title_check is not None:
        err = True
        for key in title_check:
            errors[key] = title_check[key]

    description_check = validate_description(request)
    if description_check is not None:
        err = True
        for key in description_check:
            errors[key] = description_check[key]

    category_check = validate_category(request)
    if category_check is not None:
        err = True
        for key in category_check:
            errors[key] = category_check[key]

    start_check = validate_start_date(request)
    if start_check is not None:
        err = True
        for key in start_check:
            errors[key] = start_check[key]

    end_check = validate_end_date(request)
    if end_check is not None:
        err = True
        for key in end_check:
            errors[key] = end_check[key]

    number_of_hours_check = validate_number_of_hours(request)
    if number_of_hours_check is not None:
        err = True
        for key in number_of_hours_check:
            errors[key] = number_of_hours_check[key]

    cost_of_services_check = validate_cost_of_services(request)
    if cost_of_services_check is not None:
        err = True
        for key in cost_of_services_check:
            errors[key] = cost_of_services_check[key]

    location_check = validate_location(request)
    if location_check is not None:
        err = True
        for key in location_check:
            errors[key] = location_check[key]

    requirements_check = validate_requirements(request)
    if requirements_check is not None:
        err = True
        for key in requirements_check:
            errors[key] = requirements_check[key]

    if not err:
        current_user = request.user
        category = Category.objects.get(name=request.POST['category'])
        location = Location.objects.get(name=request.POST['location'])
        per_time = request.POST['per_time'][0]
        per_cost = request.POST['per_cost'][0]
        start = request.POST['start_date']
        d, m, y = start.split('/')
        start_date = datetime.date(year=int(y), month=int(m), day=int(d))
        end = request.POST['end_date']
        d, m, y = end.split('/')
        end_date = datetime.date(year=int(y), month=int(m), day=int(d))
        number_of_hours = request.POST['number_of_hours']
        cost_of_services = request.POST['cost_of_services']
        job = Job.create(name=request.POST['title'],
                         description=request.POST.get('description'),
                         user=current_user,
                         publish_date=datetime.datetime.now(),
                         start_date=start_date,
                         end_date=end_date,
                         location=location,
                         number_of_hours=number_of_hours,
                         per_time=per_time,
                         cost_of_services=cost_of_services,
                         per_cost=per_cost,
                         requirements=request.POST['requirements'],
                         category=category)
        job.save()


        notification_message = current_user.username + " has added a new " + category.name + " job named " + job.name
        for interest in Interest.objects.filter(category=category):
            owner_profile = interest.profile
            if owner_profile.location == location and owner_profile.user != current_user:
                Notification.objects.create(user=owner_profile, job=job, notification_link='/job/' + str(job.id),receive_date=datetime.datetime.now(),
                                    is_read=False, notification_text=notification_message)

        return JsonResponse({'': ''})
    else:
        return JsonResponse(errors)


def editify(err_dict):
    clone = {}
    for key, value in err_dict.items():
        if key.startswith('add'):
            clone['edit' + key[3:]] = value
        else:
            clone[key] = value
    return clone

def uneditify(request):
    result = PostMock()
    for key, value in request.POST.items():
        if key.startswith('edit'):
            result.POST[key[5:]] = value
        else:
            result.POST[key] = value
    return result

def edit_job_ajax(request):
    err = False
    errors = {}
    mock = uneditify(request)

    print(mock.POST)

    try:
        job_id = int(request.POST['job_id'])
    except ValueError:
        return JsonResponse({'edit-requirements-error': 'Invalid job'})

    job = Job.objects.get(pk=job_id, user=request.user)
    if not job:
        return JsonResponse({'edit-requirements-error': 'You are not authorized to edit this job'})

    empty_check = check_add_fields(mock)
    if empty_check is not None:
        return JsonResponse(editify(empty_check))

    title_check = validate_title(mock)
    if title_check is not None:
        err = True
        for key in title_check:
            errors[key] = title_check[key]

    description_check = validate_description(mock)
    if description_check is not None:
        err = True
        for key in description_check:
            errors[key] = description_check[key]

    category_check = validate_category(mock)
    if category_check is not None:
        err = True
        for key in category_check:
            errors[key] = category_check[key]

    end_check = validate_end_date(mock)
    if end_check is not None:
        err = True
        for key in end_check:
            errors[key] = end_check[key]

    number_of_hours_check = validate_number_of_hours(mock)
    if number_of_hours_check is not None:
        err = True
        for key in number_of_hours_check:
            errors[key] = number_of_hours_check[key]

    cost_of_services_check = validate_cost_of_services(mock)
    if cost_of_services_check is not None:
        err = True
        for key in cost_of_services_check:
            errors[key] = cost_of_services_check[key]

    location_check = validate_location(mock)
    if location_check is not None:
        err = True
        for key in location_check:
            errors[key] = location_check[key]

    requirements_check = validate_requirements(mock)
    if requirements_check is not None:
        err = True
        for key in requirements_check:
            errors[key] = requirements_check[key]

    if not err:
        job.description = request.POST.get('edit_description')
        start = request.POST['edit_start_date']
        d, m, y = start.split('/')
        job.start_date = datetime.date(year=int(y), month=int(m), day=int(d))
        end = request.POST['edit_end_date']
        d, m, y = end.split('/')
        job.end_date = datetime.date(year=int(y), month=int(m), day=int(d))
        job.number_of_hours = request.POST['edit_number_of_hours']
        job.per_time = request.POST['edit_per_time']
        job.cost_of_services = request.POST['edit_cost_of_services']
        job.per_cost = request.POST['edit_per_cost']
        job.location = Location.objects.get(name=request.POST['edit_location'])
        job.publish_date=datetime.datetime.now()
        job.requirements=request.POST['edit_requirements']

        job.save()

        notification_message = request.user.username + " has updated a job named " + job.name
        for interest in Interest.objects.filter(category=job.category):
            owner_profile = interest.profile
            if owner_profile.location == job.location and owner_profile.user != request.user:
                Notification.objects.create(user=owner_profile, job=job, notification_link='/job/' + str(job.id), receive_date=datetime.datetime.now(),
                                    is_read=False, notification_text=notification_message)

        return JsonResponse({'': ''})
    else:
        return JsonResponse(editify(errors))


def job_delete(request, job_id):
    job = Job.objects.get(pk=job_id)
    if job and request.user == job.user:
        job.delete()
    else:
        raise Http404('Job could not be found or you don\'t have permission')
    return redirect('/jobs')

def job_review_send_ajax(request, job_id):
    if 'stars' not in request.POST or request.POST['stars'] == '0':
        return JsonResponse({'msg': 'Missing rating'})

    stars = None
    try:
        stars = int(request.POST['stars'])
        if stars < 1 or stars > 5:
            raise ValueError('Invalid range')
    except ValueError:
        return JsonResponse({'msg': 'Invalid rating'})

    if not Job.objects.filter(pk=job_id).count():
        return JsonResponse({'msg': 'Invalid job!'})

    job = Job.objects.get(pk=job_id)

    if Review.objects.filter(user_reviewing=request.user, job=job).count():
        return JsonResponse({'msg': 'You already reviewed this job'})


    profile = Profile.objects.get(user=request.user)
    review = None
    if profile == job.profile:
        review = Review.create(
            user_reviewing=request.user,
            user_reviewed=job.user_chosen.user,
            job=job,
            stars=stars,
            timestamp=datetime.datetime.now(),
            comment=request.POST['comment'],
            employer_review=False
        )
    elif profile == job.user_chosen:
        review = Review.create(
            user_reviewing=request.user,
            user_reviewed=job.user,
            job=job,
            stars=stars,
            timestamp=datetime.datetime.now(),
            comment=request.POST['comment'],
            employer_review=True
        )
    else:
        return JsonResponse({'msg': 'You are not authorized to send a review for this job'})
    review.save()
    return JsonResponse({'msg': 'success'})
