# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import time
from django.http import HttpResponse, Http404, HttpResponseForbidden
from django.shortcuts import redirect, get_object_or_404
# Create your views here.
from django.template import loader

from web import WORK_DIRECTORY
from .filter import *
from .forms import *
from .models import *
from django.utils.dateparse import *

orders = ["-", "", "-"]
criterias = ["id", "name", "start_date", "end_date", "number of hours", "total of money received"]


def get_menu(request):
    context = {
        'login_form': LoginForm(),
        'register_form': RegisterForm()
    }
    template = loader.get_template('menu.html')
    return template.render(context, request)


def home(request):
    if os.getcwd() != WORK_DIRECTORY:
        os.chdir(WORK_DIRECTORY)
    content = loader.get_template('home.html')
    content_context = {
        'current_timestamp': int(time.time())
    }
    context = {
        'menu': get_menu(request),
        'content': content.render(content_context, request)
    }
    template = loader.get_template('template.html')
    return HttpResponse(template.render(context, request))


def sort(order, criteria):
    if (order == 0) & (criteria == 0):
        return Job.objects.order_by(orders[order] + criterias[criteria])

    if (criteria == 4):
        jobs = list(Job.objects.all())
        hours_list = []
        pk_list = []
        for job in jobs:
            if (job.per_time == "d"):
                nr_days = (job.end_date - job.start_date).days
                hours_list.append((job.number_of_hours * nr_days, job.id))
            if (job.per_time == "w"):
                nr_weeks = (job.end_date - job.start_date).days / 7.0
                hours_list.append((job.number_of_hours * nr_weeks, job.id))
            if (job.per_time == "m"):
                nr_moths = (job.end_date - job.start_date).days / 30.0
                hours_list.append((job.number_of_hours * nr_moths, job.id))
            if (job.per_time == "t"):
                hours_list.append((job.number_of_hours, job.id))
        if (order == 2):
            hours_list.sort(key=lambda tup: tup[0], reverse=True)
        else:
            hours_list.sort(key=lambda tup: tup[0])
        for tuple in hours_list:
            pk_list.append(tuple[1])
        clauses = ' '.join(['WHEN web_job.id=%s THEN %s' % (pk, i) for i, pk in enumerate(pk_list)])
        ordering = 'CASE %s END' % clauses
        return Job.objects.filter(pk__in=pk_list).extra(
            select={'ordering': ordering}, order_by=('ordering',))

    if (criteria == 5):
        jobs = list(Job.objects.all())
        prices_list = []
        pk_list = []
        for job in jobs:
            money = job.cost_of_services
            if (job.per_cost == "h"):
                if (job.per_time == "d"):
                    nr_days = (job.end_date - job.start_date).days
                    hours = job.number_of_hours * nr_days
                if (job.per_time == "w"):
                    nr_weeks = (job.end_date - job.start_date).days / 7.0
                    hours = job.number_of_hours * nr_weeks
                if (job.per_time == "m"):
                    nr_months = (job.end_date - job.start_date).days / 30.0
                    hours = job.number_of_hours * nr_months
                if (job.per_time == "t"):
                    hours = job.number_of_hours
                prices_list.append((hours * money, job.id))
            if (job.per_cost == "d"):
                nr_days = (job.end_date - job.start_date).days
                prices_list.append((nr_days * money, job.id))
            if (job.per_cost == "w"):
                nr_weeks = (job.end_date - job.start_date).days / 7.0
                prices_list.append((nr_weeks * money, job.id))
            if (job.per_cost == "m"):
                nr_moths = (job.end_date - job.start_date).days / 30.0
                prices_list.append((nr_moths * money, job.id))
            if (job.per_cost == "t"):
                prices_list.append((money, job.id))

        if (order == 2):
            prices_list.sort(key=lambda tup: tup[0], reverse=True)
        else:
            prices_list.sort(key=lambda tup: tup[0])
        for tuple in prices_list:
            pk_list.append(tuple[1])
        clauses = ' '.join(['WHEN web_job.id=%s THEN %s' % (pk, i) for i, pk in enumerate(pk_list)])
        ordering = 'CASE %s END' % clauses
        return Job.objects.filter(pk__in=pk_list).extra(
            select={'ordering': ordering}, order_by=('ordering',))

    return Job.objects.order_by(orders[order] + criterias[criteria])


def deactivate_expired_jobs(jobs_list):
    now = datetime.datetime.now().date()
    for job in jobs_list:
        if job.end_date < now:
            job.is_available = False
            job.save()


def jobs(request):
    jobs_page = 1
    job_order = 0
    job_criteria = 0
    categories_list = []
    locations_list = []
    min_hours = 0
    max_hours = 0
    per_time = ''
    if 'page' in request.GET:
        jobs_page = request.GET['page']
    try:
        jobs_page = int(jobs_page)
    except ValueError:
        raise Http404("Job page could not be found")

    if 'order' in request.GET:
        job_order = request.GET['order']
        job_order = int(job_order)
        if 'criteria' in request.GET:
            job_criteria = request.GET['criteria']
            job_criteria = int(job_criteria)
            job_list = sort(job_order, job_criteria)
        else:
            job_list = sort(job_order, 0)
    else:
        job_list = sort(0, 0)
    deactivate_expired_jobs(job_list)
    job_list = filter_jobs(request, job_list)

    if 'filter_category' in request.GET:
        categories_list = request.GET.getlist('filter_category')

    if 'filter_location' in request.GET:
        locations_list = request.GET.getlist('filter_location')

    #### FOR FILTERS WRITE CODE HERE
    '''
    Filter job_list in filter.py
    For example:
    job_list = filter_category(job_list) , where filter_category would be function in filter.py (does not exist yet)
    '''

    if 'username' in request.GET:
        user = User.objects.get(username=request.GET['username'])
        if user:
            job_list = filter_user(job_list, user)

    if '0' not in locations_list:
        for location in locations_list:
            try:
                location = int(location)
            except ValueError:
                raise Http404("Location could not be found")
            check = get_object_or_404(Location, pk=int(location))
        job_list = filter_location(job_list, locations_list)

    if '0' not in categories_list:
        for category in categories_list:
            try:
                category = int(category)
            except ValueError:
                raise Http404("Category could not be found")
            check = get_object_or_404(Category, pk=int(category))
        job_list = filter_category(job_list, categories_list)

    if "searchByHours" in request.GET and 'filter_per_time' in request.GET:
        min_hours, max_hours = None, None
        if 'filter_min_hours' in request.GET:
            min_hours = request.GET['filter_min_hours']
        if 'filter_max_hours' in request.GET:
            max_hours = request.GET['filter_max_hours']
        per_time = request.GET['filter_per_time']

        try:
            min_hours = int(min_hours)
            max_hours = int(max_hours)
            job_list = filter_jobs_hours(job_list, min_hours, max_hours, per_time)
        except ValueError:
            raise Http404("Job page could not be found")

    if "searchByCost" in request.GET:
        min_cost, max_cost, per_cost, voluntary = 0, 99999999999, None, None
        if 'filter_min_cost' in request.GET:
            min_cost = request.GET['filter_min_cost']
        if 'filter_max_cost' in request.GET:
            max_cost = request.GET['filter_max_cost']
        if 'filter_per_cost' in request.GET:
            per_cost = request.GET['filter_per_cost']
        if 'filter_voluntary' in request.GET:
            voluntary = request.GET['filter_voluntary']
        print(min_cost, max_cost)
        for elem in request:
            print(elem.value)
        try:
            min_cost = int(min_cost)
            max_cost = int(max_cost)
            job_list = filter_jobs_cost(job_list, min_cost, max_cost, per_cost, voluntary)
        except ValueError:
            raise Http404("Job page could not be found")

    if "searchByInterval" in request.GET:
        start_date, end_date = None, None
        if 'filter_start_date' in request.GET:
            start_date = request.GET['filter_start_date']
        if 'filter_end_date' in request.GET:
            end_date = request.GET['filter_end_date']

        try:
            start_date = datetime.datetime.strptime(start_date, '%d/%m/%Y')
            end_date = datetime.datetime.strptime(end_date, '%d/%m/%Y')
            # print("START: "+str(start_date)+"\n")
            # print("END: "+str(end_date)+"\n")
            if start_date > end_date:
                raise ValueError
            job_list = filter_jobs_interval(job_list, start_date, end_date)
        except ValueError:
            raise Http404("Invalid date format")

    #### UNTIL HERE
    # Leave this as it is, as we need the paged view
    max_page, job_list, page_buttons = filter_content(job_list, jobs_page)
    if job_list is None:
        raise Http404("Job page could not be found")

    content = loader.get_template('jobs.html')
    paged = loader.get_template('paged.html')

    for j in job_list:
        profile = j.profile
        reviews = Review.objects.filter(user_reviewed=profile.user, employer_review=True)
        if reviews.count() == 0:
            profile.average_employer_rating = None
        else:
            profile.average_employer_rating = sum([review.stars for review in reviews]) / reviews.count()

    profile = None
    if request.user.is_authenticated:
        profile = get_object_or_404(Profile, user=request.user)

    paged_context = {
        'page': jobs_page,
        'max_page': max_page,
        'page_buttons': page_buttons,
        'prev_page': jobs_page - 1,
        'next_page': jobs_page + 1
    }

    content_context = {
        'user': request.user,
        'profile': profile,
        'jobs': job_list,
        'add_form': AddJobForm(),
        'edit_form': EditJobForm(),
        'paged': paged.render(paged_context, request),
        'searchByHoursForm': SearchByHoursForm(),
        'searchByIntervalForm': SearchByIntervalForm(),
        'searchByCostForm': SearchByCostForm(),
        'category_form': CategorySearchForm(),
        'location_form': LocationSearchForm(),
        'order_form': OrderSearchForm(),
        'criteria_form': CriteriaSearchForm(),
        'current_timestamp': int(time.time())
    }
    context = {
        'menu': get_menu(request),
        'content': content.render(content_context, request)
    }
    template = loader.get_template('template.html')
    return HttpResponse(template.render(context, request))


def seekers(request):
    seekers_page = 1
    if 'page' in request.GET:
        seekers_page = request.GET['page']
    try:
        seekers_page = int(seekers_page)
    except ValueError:
        raise Http404("Users page could not be found")
    seekers_list = Profile.objects.order_by('-id')

    max_page, seekers_list, page_buttons = filter_content(seekers_list, seekers_page)
    if seekers_list is None:
        raise Http404("Users page could not be found")

    content = loader.get_template('seekers.html')
    paged = loader.get_template('paged.html')
    paged_context = {
        'page': seekers_page,
        'max_page': max_page,
        'page_buttons': page_buttons,
        'prev_page': seekers_page - 1,
        'next_page': seekers_page + 1
    }

    for seeker in seekers_list:
        reviews = Review.objects.filter(user_reviewed=seeker.user, employer_review=False)
        if reviews.count() == 0:
            seeker.average_applicant_rating = None
        else:
            seeker.average_applicant_rating = sum([review.stars for review in reviews]) / reviews.count()
        seeker.save(ignore_profile_picture=True)

    content_context = {
        'seekers': [[seekers_list[x] for x in range(y, len(seekers_list), 4)] for y in range(4)],
        'paged': paged.render(paged_context, request),
        'current_timestamp': int(time.time())
    }
    context = {
        'menu': get_menu(request),
        'content': content.render(content_context, request)
    }
    template = loader.get_template('template.html')
    return HttpResponse(template.render(context, request))


def edit_profile(request):
    if not request.user.is_authenticated:
        redirect('/home')
    content = loader.get_template('edit.html')
    profile = Profile.objects.get(user=request.user)
    content_context = {
        'user': request.user,
        'profile': profile,
        'form': EditProfileForm(user=request.user),
        'interests_form': EditInterestForm(user=request.user),
        'location_id': profile.location.id if profile.location else 0,
        'current_timestamp': int(time.time())
    }
    if os.path.exists(profile.profile_picture.url):
        content_context['has_picture'] = 'True'
    context = {
        'menu': get_menu(request),
        'content': content.render(content_context, request)
    }
    template = loader.get_template('template.html')
    return HttpResponse(template.render(context, request))


def edit_interest_profile(request):
    if not request.user.is_authenticated:
        redirect('/home')
    content = loader.get_template('edit.html')
    content_context = {
        'user': request.user,
        'profile': Profile.objects.get(user=request.user),
        'form': EditInterestForm(user=request.user),
        'current_timestamp': int(time.time())
    }
    context = {
        'menu': get_menu(request),
        'content': content.render(content_context, request)
    }
    template = loader.get_template('template.html')
    return HttpResponse(template.render(context, request))


def job(request, job_id):
    job_id = int(job_id)
    job = get_object_or_404(Job, pk=job_id)
    applicants = [profile.user for profile in job.applicants.all()]
    if not job.is_available and request.user not in applicants and request.user != job.user:
        raise Http404("Job was not found!")

    seekers_page = 1
    if 'page' in request.GET:
        seekers_page = request.GET['page']
    try:
        seekers_page = int(seekers_page)
    except ValueError:
        raise Http404("Users page could not be found")
    applicants_list = job.applicants.all()

    max_page, applicants_list, page_buttons = filter_content(applicants_list, seekers_page)
    if applicants_list is None:
        raise Http404("Could not load applicants")

    reviews = Review.objects.filter(user_reviewed=job.profile.user, employer_review=True)
    if reviews.count() == 0:
        job.profile.average_employer_rating = None
    else:
        job.profile.average_employer_rating = sum([review.stars for review in reviews]) / reviews.count()
    job.profile.save(ignore_profile_picture=True)

    paged = loader.get_template('paged.html')
    content = loader.get_template('job.html')
    paged_context = {
        'page': seekers_page,
        'max_page': max_page,
        'page_buttons': page_buttons,
        'prev_page': seekers_page - 1,
        'next_page': seekers_page + 1
    }
    content_context = {
        'job': job,
        'paged': paged.render(paged_context, request),
        'applicants': [[applicants_list[x] for x in range(y, len(applicants_list), 4)] for y in range(4)],
        'applicants_no': len(applicants),
        'edit_form': EditJobForm(),
        'reviewable': True if job.end_date < datetime.date.today() and not job.is_available and
                              Review.objects.filter(user_reviewing=request.user, job=job).count() == 0 and (
                                  (job.user == request.user) or
                                  (job.user_chosen and job.user_chosen.user == request.user)
                              ) else False,
        'current_timestamp': int(time.time())
    }
    # if not content_context['reviewable']:
    content_context['review_form'] = ReviewForm()

    context = {
        'menu': get_menu(request),
        'content': content.render(content_context, request)
    }
    template = loader.get_template('template.html')
    return HttpResponse(template.render(context, request))


def job_choose_applicant(request, job_id):
    job_id = int(job_id)

    if "user" not in request.POST:
        raise Http404("User was not found")

    profile = get_object_or_404(Profile, pk=int(request.POST["user"]))
    job = get_object_or_404(Job, pk=job_id)

    job.user_chosen = profile
    job.is_available = False
    job.save()

    notification_message = "You have been selected for the job " + job.name
    Notification.objects.create(user=profile, job=job, notification_link='/job/' + str(job.id),
                                receive_date=datetime.datetime.now(),
                                is_read=False, notification_text=notification_message)

    return redirect('/job/' + str(job_id))


def job_deselect_applicant(request, job_id):
    job_id = int(job_id)

    if "user" not in request.POST:
        raise Http404("User was not found")

    profile = get_object_or_404(Profile, pk=int(request.POST["user"]))
    job = get_object_or_404(Job, pk=job_id)

    job.user_chosen = None
    job.is_available = True
    job.save()

    notification_message = "You have been de-selected from the job " + job.name
    Notification.objects.create(user=profile, job=job, notification_link='/job/' + str(job.id),
                                receive_date=datetime.datetime.now(),
                                is_read=False, notification_text=notification_message)

    return redirect('/job/' + str(job_id))


def job_apply(request, job_id):
    job_id = int(job_id)

    if "user" not in request.POST:
        raise Http404("Could not apply for the job!")

    user = get_object_or_404(User, id=int(request.POST["user"]))
    profile = get_object_or_404(Profile, user=user)

    job = get_object_or_404(Job, pk=job_id)
    application = Application.objects.filter(user=profile, job=job)
    if len(application) > 0:
        application[0].delete()
    else:
        Application.objects.create(user=profile, job=job)

        owner_profile = get_object_or_404(Profile, user=job.user)
        applicant_name = profile.user.first_name + " " + profile.user.last_name if len(
            profile.user.first_name) > 0 and len(
            profile.user.last_name) > 0 else profile.user.username
        notification_message = applicant_name + " has applied for your job: " + job.name
        Notification.objects.create(user=owner_profile, job=job, notification_link='/job/' + str(job.id),
                                    receive_date=datetime.datetime.now(),
                                    is_read=False, notification_text=notification_message)

    return redirect('/job/' + str(job_id))


def handle_notification(request, notification_id):
    notification = Notification.objects.get(id=notification_id)
    notification.is_read = True
    notification.save()
    if not notification.notification_link:
        return redirect('/job/' + str(notification.job.id))
    else:
        return redirect(notification.notification_link)


def notifications(request):
    notifications_page = 1
    if 'page' in request.GET:
        notifications_page = request.GET['page']

    try:
        notifications_page = int(notifications_page)
    except ValueError:
        raise Http404("Notification page could not be found")

    user_profile = Profile.objects.get(user=request.user)
    notification_list = Notification.objects.filter(user=user_profile).order_by('-id')
    max_page, notification_list, page_buttons = filter_content(notification_list, notifications_page)
    if notification_list is None:
        raise Http404("Notification page could not be found")

    content = loader.get_template('notifications.html')
    paged = loader.get_template('paged.html')

    paged_context = {
        'page': notifications_page,
        'max_page': max_page,
        'page_buttons': page_buttons,
        'prev_page': notifications_page - 1,
        'next_page': notifications_page + 1
    }

    content_context = {
        'notifications': notification_list,
        'paged': paged.render(paged_context, request),
        'current_timestamp': int(time.time())
    }
    context = {
        'menu': get_menu(request),
        'content': content.render(content_context, request)
    }
    template = loader.get_template('template.html')
    return HttpResponse(template.render(context, request))


def job_review(request, job_id):
    job = get_object_or_404(Job, pk=job_id)

    content = loader.get_template('review.html')
    content_context = {
        'job': job,
        'user': request.user,
        'form': ReviewForm(),
        'current_timestamp': int(time.time())
    }
    context = {
        'menu': get_menu(request),
        'content': content.render(content_context, request)
    }
    template = loader.get_template('template.html')
    return HttpResponse(template.render(context, request))


def reviews(request, username):
    reviews_page = 1
    if 'page' in request.GET:
        reviews_page = request.GET['page']

    try:
        reviews_page = int(reviews_page)
    except ValueError:
        raise Http404("Review page could not be found!")

    user = get_object_or_404(User, username=username)

    review_list = Review.objects.filter(user_reviewed=user).order_by('-id')
    max_page, review_list, page_buttons = filter_content(review_list, reviews_page)
    if review_list is None:
        raise Http404("Notification page could not be found")

    content = loader.get_template('reviews.html')
    paged = loader.get_template('paged.html')

    paged_context = {
        'page': reviews_page,
        'max_page': max_page,
        'page_buttons': page_buttons,
        'prev_page': reviews_page - 1,
        'next_page': reviews_page + 1
    }

    content_context = {
        'reviews': review_list,
        'paged': paged.render(paged_context, request),
        'current_timestamp': int(time.time())
    }
    context = {
        'menu': get_menu(request),
        'content': content.render(content_context, request)
    }
    template = loader.get_template('template.html')
    return HttpResponse(template.render(context, request))


def upload_pic(request):
    if request.method == 'POST':
        form = UploadProfilePictureForm(request.POST, request.FILES)
        if form.is_valid():
            # super important here: if work directory was not properly set, set it now before saving image to wrong place
            if os.getcwd() != WORK_DIRECTORY:
                os.chdir(WORK_DIRECTORY)
            profile = Profile.objects.get(user=request.user)
            profile.profile_picture = form.cleaned_data['image']
            profile.save()
            return redirect('/profile/' + request.user.username + '/edit')
    return HttpResponseForbidden('Allowed only via POST')


def render_profile(request, username, page, rendered_subpage):
    if username == '':
        return redirect('/profile/' + str(request.user.username))
    user = None
    try:
        user = User.objects.get(username=username)
    except:
        return redirect('/home')
    content = loader.get_template('profile.html')
    user_profile = Profile.objects.get(user=user)
    content_context = {
        'profile': user_profile,
        'user': user,
        'p_interests': False,
        'p_jobs': False,
        'p_reviews': False,
        'p_notifications': False,
        'p_edit': False,
        'rendered_subpage': rendered_subpage,
        'current_timestamp': int(time.time())
    }
    content_context[page] = True

    #print(content_context)
    context = {
        'menu': get_menu(request),
        'content': content.render(content_context, request)
    }
    template = loader.get_template('template.html')
    return HttpResponse(template.render(context, request))


def profile(request, username):
    return redirect('/profile/{0}/jobs'.format(username))


def profile_dashboard(request, username):
    if request.user.username != username:
        raise Http404("Whooops!")
    profile_subpage = loader.get_template('profile_interests.html')

    user = get_object_or_404(User, username=username)
    profile = Profile.objects.get(user=user)
    jobs_list = []

    for interest in profile.interests.all():
        jobs_list += Job.objects.filter(category=interest)
    jobs_list = [job for job in jobs_list if job.user.id != request.user.id and job.is_available is True]
    jobs_list.sort(key=id, reverse=True)

    jobs_page = 1
    if 'page' in request.GET:
        jobs_page = request.GET['page']
    try:
        jobs_page = int(jobs_page)
    except ValueError:
        raise Http404("Jobs page could not be found")

    if jobs_list is None:
        raise Http404("Could not load jobs")
    max_page, jobs_list, page_buttons = filter_content(jobs_list, jobs_page)
    if jobs_list is None:
        raise Http404("Could not load jobs")

    paged = loader.get_template('paged.html')
    paged_context = {
        'page': jobs_page,
        'max_page': max_page,
        'page_buttons': page_buttons,
        'prev_page': jobs_page - 1,
        'next_page': jobs_page + 1
    }

    context = {
        'jobs': jobs_list,
        'paged': paged.render(paged_context, request),
    }
    return render_profile(request, username, 'p_interests', profile_subpage.render(context, request))


def profile_jobs(request, username):
    profile_subpage = loader.get_template('profile_jobs.html')

    user = get_object_or_404(User, username=username)
    jobs_list = Job.objects.filter(user=user).order_by('-id')

    jobs_page = 1
    if 'page' in request.GET:
        jobs_page = request.GET['page']
    try:
        jobs_page = int(jobs_page)
    except ValueError:
        raise Http404("Jobs page could not be found")

    max_page, jobs_list, page_buttons = filter_content(jobs_list, jobs_page)
    if jobs_list is None:
        raise Http404("Could not load jobs")

    paged = loader.get_template('paged.html')
    paged_context = {
        'page': jobs_page,
        'max_page': max_page,
        'page_buttons': page_buttons,
        'prev_page': jobs_page - 1,
        'next_page': jobs_page + 1
    }

    context = {
        'jobs': jobs_list,
        'paged': paged.render(paged_context, request),
        'edit_form': EditJobForm(),
    }
    return render_profile(request, username, 'p_jobs', profile_subpage.render(context, request))


def profile_reviews(request, username):
    profile_subpage = loader.get_template('profile_reviews.html')
    user = get_object_or_404(User, username=username)
    review_list = Review.objects.filter(user_reviewed=user).order_by('-id')
    context = {
        'reviews': review_list
    }
    return render_profile(request, username, 'p_reviews', profile_subpage.render(context, request))


def profile_notifications(request, username):
    profile_subpage = loader.get_template('profile_notifications.html')
    user_profile = Profile.objects.get(user=request.user)
    notification_list = Notification.objects.filter(user=user_profile).order_by('-id')
    if request.user.username != username:
        raise Http404("Whooops!")
    context = {
        'notifications': notification_list,
        'profile': user_profile
    }
    return render_profile(request, username, 'p_notifications', profile_subpage.render(context, request))


def profile_edit(request, username):
    profile_subpage = loader.get_template('profile_edit.html')
    if request.user.username != username:
        raise Http404("Whoops!")
    context = {
        'profile': Profile.objects.get(user=request.user),
        'form': EditProfileForm(user=request.user),
        'interest_form': EditInterestForm(user=request.user),
        'user_interests': Interest.objects.filter(profile=Profile.objects.get(user=request.user))
    }
    return render_profile(request, username, 'p_edit', profile_subpage.render(context, request))


'''
def profile(request, username):
    if username == '':
        return redirect('/profile/' + str(request.user.username))
    user = None
    try:
        user = User.objects.get(username=username)
    except:
        return redirect('/home')
    content = loader.get_template('profile.html')
    user_profile = Profile.objects.get(user=user)
    jobs_list = Job.objects.filter(user=user).order_by('-id')[:5]
    interests = Interest.objects.filter(profile=user_profile).order_by('-id')[:5]
    notifications = Notification.objects.filter(user=user_profile, is_read=False).order_by('-id')[:5]
    content_context = {
        'user': user,
        'profile': user_profile,
        'jobs': jobs_list,
        'interests': interests,
        'notifications': notifications,
    }
    if user == request.user:
        content_context['own_profile'] = 'True'
    if os.path.exists(os.path.join('web', user_profile.profile_picture.url)):
        content_context['has_picture'] = 'True'
    content_context['zip'] = zip_longest(content_context['jobs'], content_context['interests'])
    context = {
        'menu': get_menu(request),
        'content': content.render(content_context, request)
    }
    template = loader.get_template('template.html')
    return HttpResponse(template.render(context, request))
'''
