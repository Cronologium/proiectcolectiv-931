# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

# Register your models here.
from .models import Job, Profile, Category, Location, Notification, Application, Interest, Review

admin.site.register(Job)
admin.site.register(Profile)
admin.site.register(Category)
admin.site.register(Location)
admin.site.register(Notification)
admin.site.register(Application)
admin.site.register(Interest)
admin.site.register(Review)


