function check_notifications(repeat) {
    $.ajax({
        url: '/check_notifications',
        data: {'check': 'true'},
        success: function(data) {
            if ('notifications' in data)
            {
                $('#notification-item').html('<p id="notifications-button">' + data['notifications'] + '</p>');
                $('#notification-item').show();

                $('#notification-context').html(data['newest_notification_text']);
                $('#notification-context').click(function () {
                    window.location.href = "/handle_notification/" + data['newest_notification_id'];
                    $('#notification-bar').hide();
                });
                $('#notification-bar').find("button").click(function (){
                    var handle_url = '/handle_notification/' + data['newest_notification_id'];
                    $.ajax({
                        url: handle_url,
                        type: 'GET',
                        complete: function(data) {
                            $("#notification-bar").hide();
                            check_notifications(false);
                        },
                    });
                });
                $('#notification-bar').show();
            }
            else {
                $('#notification-item').hide();
                $('#notification-bar').hide();
            }
        },
        complete: function() {
            // Schedule the next request when the current one's complete
            if (repeat)
                setTimeout(check_notifications, 10000, true);
        }
    });
}

$(document).ready(function () {
    check_notifications(true);
});