
$( document ).ready(function() {
  var slideIndex = 1;
  showSlides(slideIndex);

  function plusSlides(n) {
    showSlides(slideIndex += n);
  }

  function currentSlide(n) {
    showSlides(slideIndex = n);
  }

  function showSlides(n) {
    var i;
    var slides = document.getElementsByClassName("slide");
    //console.log(slides.length)
    var dots = document.getElementsByClassName("dot");
    if (n > slides.length) {slideIndex = 1}
    if (n < 1) {slideIndex = slides.length}
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
    }
    for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" active", "");
    }
    slides[slideIndex-1].style.display = "block";
    dots[slideIndex-1].className += " active";
  }

  $('.slideshow_container').on('click','.prev',function(){
      plusSlides(-1);
  });
  $('.slideshow_container').on('click','.next',function(){
      plusSlides(1);
  });

  $("#slide1").click(function() {
    slideIndex=1;
    showSlides(1);
  });

    $("#slide2").click(function() {
    slideIndex=2;
    showSlides(2);
  });

  $("#slide3").click(function() {
    slideIndex=3;
    showSlides(3);
  });
});