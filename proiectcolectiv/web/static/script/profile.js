$(document).ready(function () {
    update_height();

    /*$('.seen-checkbox').each(function(){
        $(this).click(function(){
            var notification_id = $(this).val();
            var handle_url = '/handle_notification/' + notification_id;
            $(this).attr("disabled", true);
            $.ajax({
                url: handle_url,
                type: 'GET',
                complete: function() {
                    check_notifications(false);
                }
            })
        });
    });*/
    $('.notification-data').each(function() {
        $(this).hover(function(){
            var notif_object = this;
            var eye = $(this).find(".seen-checkbox");
            if (eye.length) {
                eye = eye[0];
                notif_id = $(eye).find("input").val();
                var handle_url = '/handle_notification/' + notif_id;
                if (notif_id) {
                    console.log(handle_url);
                    $.ajax({
                        url: handle_url,
                        type: 'GET',
                        complete: function () {
                            check_notifications(false);
                            $(eye).html("<span class=\"seen-status\">\n" +
                                "                                <i class=\"fa fa-eye\"></i>\n" +
                                "                            </span>");
                            $(notif_object).removeClass("is-primary");
                        }
                    });
                }
            }
        });
    })
});

function update_height(){
    var height = $("#notifications").height();
    if (height === undefined){
        var offersHeight = $(".offers-interests").height();
        if (offersHeight === undefined)
            $(".form").height(375);
        else
            $(".form").height(Math.max(375, offersHeight + 100));
    }
    else{
        $(".form").height(height + 500);
    }
}
